var Criminal_Night = Criminal_Night || {};

Criminal_Night.About = function(){};

//setting game configuration and loading the assets for the loading screen
Criminal_Night.About.prototype = {
  preload: function() {
    this.game.load.spritesheet('button-back', 'assets/back.png', 400, 200);
  },
  create: function() {
    var box = this.game.add.sprite(0,200,this.game.make.bitmapData(this.game.width, this.game.height));
        console.log("entered the menu");
		box.addChild(this.game.add.text(300, 0, "About: ",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(500, 150, "this is a game from course: cse380",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(500, 300, "team member is:",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(500, 450, "Chenxing He",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(500, 600, "Shenhui Yu",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(500, 750, "David Szenczewski",{fontSize:'100px',fill:'#fff'}));
		box.alpha = 0;
		this.game.add.tween(box).to({alpha:1,y:150},3000,"Linear",true);

		var buttonClose = this.game.add.button((this.game.width/2)-200, 1400, 'button-back', null, this, 0, 1, 0, 1);
		buttonClose.events.onInputDown.add(function(){
      Criminal_Night.game.state.start('Menu');
    });
    
  }
};