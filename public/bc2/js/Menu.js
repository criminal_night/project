var Criminal_Night = Criminal_Night || {};

Criminal_Night.Menu = function(){};

//setting game configuration and loading the assets for the loading screen
Criminal_Night.Menu.prototype = {
  preload: function() {
    this.game.load.spritesheet('button-start', 'assets/begin.png', 400, 200);
    this.game.load.spritesheet('button-help', 'assets/help.png', 400, 200);
    this.game.load.spritesheet('button-about', 'assets/about.png', 400, 200);
  },
  create: function() {
    var box = this.game.add.sprite(0,200,this.game.make.bitmapData(this.game.width, this.game.height));
        console.log("entered the menu");
    box.addChild(this.game.add.text(300, 0, "long long ago, the New York is ruined by criminals",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(300, 200, "People died because of the gang fighting ",{fontSize:'100px',fill:'#fff'}));
    box.addChild(this.game.add.text(300, 400, "New York lay in ruins",{fontSize:'100px',fill:'#fff'}));
		box.addChild(this.game.add.text(300, 600, "It time to save the New York！！！",{fontSize:'100px',fill:'#fff'}));
		box.alpha = 0;
		this.game.add.tween(box).to({alpha:1,y:150},3000,"Linear",true);

    var startbutton = this.game.add.button((this.game.width/2)-200, 1000, 'button-start', null, this, 0, 1, 0, 1);
    var helpbutton = this.game.add.button((this.game.width/2)-200, 1300, 'button-help', null, this, 0, 1, 0, 1);
    var aboutbutton = this.game.add.button((this.game.width/2)-200, 1600, 'button-about', null, this, 0, 1, 0, 1);
		startbutton.events.onInputDown.add(function(){
      Criminal_Night.game.state.start('Game');
    });
    
    helpbutton.events.onInputDown.add(function(){
      Criminal_Night.game.state.start('Help');
    });

    aboutbutton.events.onInputDown.add(function(){
      Criminal_Night.game.state.start('About');
    });
    //this.state.start('Game');
  }
};