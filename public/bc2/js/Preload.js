var Criminal_Night = Criminal_Night || {};

//loading the game assets
Criminal_Night.Preload = function(){};

Criminal_Night.Preload.prototype = {
  preload: function() {
    //show loading screen
    // this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadbar');
    // this.preloadBar.anchor.setTo(0.5);

    // this.load.setPreloadSprite(this.preloadBar);

    //load game assets
    this.load.tilemap('worldmap', 'assets/map/worldmap.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.image('gameTiles', 'assets/map/tiles.png');
    //this.load.spritesheet('player', 'assets/images/player.png',128,128);
    this.load.image('player', 'assets/spaceShips_001.png');
    this.load.image('otherPlayer', 'assets/enemyBlack5.png');
    this.load.image('star', 'assets/star_gold.png');

    
  },
  create: function() {
    console.log("entered the Preload");
    this.state.start('Menu');
  }
};