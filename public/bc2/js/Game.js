const firebaseConfig = {
  apiKey: "AIzaSyBLuLBmDVjcxzlqgjSXJprC87lQWRZb93Q",
  authDomain: "criminal-night.firebaseapp.com",
  databaseURL: "https://criminal-night.firebaseio.com/",
  storageBucket: "criminal-night.appspot.com"
};
firebase.initializeApp(firebaseConfig);



var Criminal_Night = Criminal_Night || {};

//title screen
Criminal_Night.Game = function(){};
Criminal_Night.Game.prototype = {
	create: function(){

		this.map = this.game.add.tilemap('worldmap');
	    this.map.addTilesetImage('tiles', 'gameTiles');
	    //create layer
	    this.backgroundlayer = this.map.createLayer('backgroundLayer');
	    this.blockedLayer = this.map.createLayer('blockedLayer');
	    //collision on blockedLayer
	    this.map.setCollisionBetween(1, 1500, true, 'blockedLayer');
	    //resizes the game world to match the layer dimensions
		this.backgroundlayer.resizeWorld();
		this.cursors = this.game.input.keyboard.createCursorKeys();
		
		var self=this;
		this.otherPlayers={};
		this.database = firebase.database().ref();
		var px=Math.floor(Math.random() * 700) + 150;
		var py=Math.floor(Math.random() * 500) + 150;

		this.currentPlayers=this.database.child("currentPlayers");
		var thisPlayer=this.currentPlayers.push({
			x:px,
			y:py,
		});

		self.player=self.game.add.sprite(px, py , 'player');
		this.game.physics.arcade.enable(this.player);
		this.game.camera.follow(this.player);



		self.player.playerId=thisPlayer.key;

		self.currentPlayers.on("child_added", function(player){
				if(player.key==self.player.playerId)
					return;
				else{
					var otherPlayer=self.game.add.sprite(player.val().x,player.val().y, 'otherPlayer');
					otherPlayer.playerId=player.key;
					self.game.physics.arcade.enable(otherPlayer);
					self.otherPlayers[player.key] = otherPlayer;
				}

			
		});
		self.currentPlayers.on("child_changed", function(moved_player){
			if(moved_player.key in self.otherPlayers){
				self.otherPlayers[moved_player.key].x = moved_player.val().x;
				self.otherPlayers[moved_player.key].y = moved_player.val().y;
			}
		});
		self.currentPlayers.on("child_removed", function(moved_player){
			if(moved_player.key in self.otherPlayers){
				self.otherPlayers[moved_player.key].destroy();
			}
		});
		this.currentPlayers.child(this.player.playerId).onDisconnect().remove((err)=> {
	        if (err) {
	            console.error('could not establish onDisconnect event', err);
	        }
	    });
		
	},
	update: function(){
		this.game.physics.arcade.collide(this.player, this.blockedLayer);
		this.player.body.velocity.x = 0;

	    if(this.cursors.up.isDown) {
	      if(this.player.body.velocity.y == 0)
	      this.player.body.velocity.y -= 500;
	    }
	    else if(this.cursors.down.isDown) {
	      if(this.player.body.velocity.y == 0)
	      this.player.body.velocity.y += 500;
	    }
	    else {
	      this.player.body.velocity.y = 0;
	    }
	    if(this.cursors.left.isDown) {
	      this.player.body.velocity.x -= 500;
	    }
	    else if(this.cursors.right.isDown) {
	      this.player.body.velocity.x += 500;
	    }
		this.currentPlayers.child(this.player.playerId).set({x:this.player.x, y:this.player.y});
	}
};