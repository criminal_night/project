const firebaseConfig = {
  apiKey: "AIzaSyBLuLBmDVjcxzlqgjSXJprC87lQWRZb93Q",
  authDomain: "criminal-night.firebaseapp.com",
  databaseURL: "https://criminal-night.firebaseio.com/",
  storageBucket: "criminal-night.appspot.com"
};
firebase.initializeApp(firebaseConfig);
var Criminal_Night = Criminal_Night || {};

//title screen
Criminal_Night.Game = function(){};
Criminal_Night.Game.prototype = {
	create: function(){

		this.map = this.game.add.tilemap('worldmap');
	    this.map.addTilesetImage('tiles', 'gameTiles');
	    //create layer
	    this.backgroundlayer = this.map.createLayer('backgroundLayer');
	    this.blockedLayer = this.map.createLayer('blockedLayer');
	    //collision on blockedLayer
	    this.map.setCollisionBetween(1, 10000, true, 'blockedLayer');

	    //resizes the game world to match the layer dimensions
		this.backgroundlayer.resizeWorld();
		this.cursors = this.game.input.keyboard.createCursorKeys();
		
		var self=this;
		this.otherPlayers={};
		this.items_in_use = this.game.add.group();
		this.items = this.game.add.group();
		this.items.enableBody = true;
		this.items_in_use.enableBody = true;



		this.database = firebase.database().ref();
		self.currentPlayers=self.database.child("currentPlayers");
		self.item_set=self.database.child("worldTool");
		self.item_in_use = self.database.child("item_in_use");
		
		var node=this.random_spawn();
		var thisPlayer=self.currentPlayers.push({
			x:node.x,
			y:node.y,
			//visible:true
		});
		
		self.player=self.game.add.sprite(node.x, node.y, 'shadow');
		self.player.playerId=thisPlayer.key;
		self.game.physics.arcade.enable(self.player);
		self.game.camera.follow(self.player);
		self.build_player_animation();
		self.player.animations.play("idle");
		self.player.selectBoxPosition = 0;
		self.player.maxspeed = 250;
		self.player.speedable = true;
		
		//generate toolbox and score box
		self.toolbox = {
			"0":null,
			"1":null,
			"2":null
		};
	    this.player.scope=0;
	    this.toolboxImage=this.game.add.image(1600,850,'toolbox');
	    this.toolboxImage.fixedToCamera=true;
	    this.selectbox=this.game.add.image(1600,850,'selectbox');
	    this.selectbox.fixedToCamera=true;
	    var scopeText = this.game.add.text(1350, 50, "Score: "+this.player.scope, { font: "100px Arial", fill: "#ffffff", align: "center" });
	    scopeText.fixedToCamera = true;

	    //space event and click event
	   	this.space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
	    this.space.onDown.add(function(){
	      console.log("space key is pressed");
	      if(this.player.selectBoxPosition<2){
	        this.selectbox.cameraOffset.x+=128;
	        this.player.selectBoxPosition++;
	      }else{
	        this.selectbox.cameraOffset.x-=2*128;
	        this.player.selectBoxPosition=0;
	      }
	    }, this); 


		//-----------
		self.currentPlayers.on("child_added", function(player){
			if(player.key==self.player.playerId)
				return;
			else{
				var otherPlayer=self.game.add.sprite(player.val().x,player.val().y, 'robot');
				self.build_bot_animation(otherPlayer);
				otherPlayer.animations.play("idle");
				otherPlayer.playerId=player.key;
				self.game.physics.arcade.enable(otherPlayer);
				self.otherPlayers[player.key] = otherPlayer;
				// if(player.val().visible==false){
				// 	otherPlayer.visible = false;
				// }

				// add new items
			}
		});
		self.currentPlayers.on("child_changed", function(moved_player){
			if(moved_player.key in self.otherPlayers){
				if(self.otherPlayers[moved_player.key].x<moved_player.val().x)
					self.otherPlayers[moved_player.key].animations.play("walking-right");
				else if(self.otherPlayers[moved_player.key].x>moved_player.val().x)
					self.otherPlayers[moved_player.key].animations.play("walking-left");
				else
					self.otherPlayers[moved_player.key].animations.play("idle");

				//self.otherPlayers[moved_player.key].visible = moved_player.visible;
				self.otherPlayers[moved_player.key].x = moved_player.val().x;
				self.otherPlayers[moved_player.key].y = moved_player.val().y;
			}
		});
		self.currentPlayers.on("child_removed", function(moved_player){
			if(moved_player.key in self.otherPlayers){
				self.otherPlayers[moved_player.key].destroy();
			}
		});


		self.currentPlayers.child(self.player.playerId).onDisconnect().remove((err)=> {
	        if (err) {
	            console.error('could not establish onDisconnect event', err);
	        }
	    });

	    self.item_set.on("value", function(data){
	    	console.log("changed");
	    	self.item_data=data.val();
	    });

	    self.item_set.on("child_added", function(item){
			/*star 20      car   21    glue   22    hood	23
				net. 24     light 25     mine.	26    shoes 27
				knife. 28
			*/ 
			if(item.val().value==20){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "star");
			}else if(item.val().value==21){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "car");
			}else if(item.val().value==22){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "glue");
			}else if(item.val().value==23){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "hood");
			}else if(item.val().value==24){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "net");
			}else if(item.val().value==25){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "light");
			}else if(item.val().value==26){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "mine");
			}else if(item.val().value==27){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "shoes");
			}else if(item.val().value==28){
				var new_item=self.items.create(item.val().column*150, item.val().row*150, "knife");
			}
			new_item.inputEnabled = true;
			//new_item.event.onInputOver.add(yourClickFunction, this);
	    	new_item.itemId=item.key;
	    });

	    self.item_set.on("child_changed", function(item){
	    	self.items.forEach(function(child){
	    		if(child.itemId==item.key){
	    			child.x=item.val().column*150;
	    			child.y=item.val().row*150;
	    		}
	    	});
	    });

	    self.item_in_use.on("child_added", function(item){
	    	if(item.val().value==24){
	    		var new_item = self.items_in_use.create(item.val().ox, item.val().oy, "net");
	    		self.game.physics.arcade.enable(new_item, Phaser.Physics.Arcade);
	    		new_item.name = item.key;
	    		self.game.physics.arcade.moveToXY(new_item, item.val().dx, item.val().dy, 600);
	    		new_item.killable = false;
	    		setInterval(function(){
	    			new_item.killable = true;
	    		}, 2000);
	    	}
	    	else if(item.val().value==22){
	    		var new_item = self.items_in_use.create(item.val().ox, item.val().oy, "glue");
	    		self.game.physics.arcade.enable(new_item, Phaser.Physics.Arcade);
	    		new_item.name = item.key;
	    		new_item.killable = false;
	    		setInterval(function(){
	    			new_item.killable = true;
	    		}, 2000);
	    	}

	    	else if(item.val().value==26){
	    		var new_item = self.items_in_use.create(item.val().ox, item.val().oy, "mine");
	    		self.game.physics.arcade.enable(new_item, Phaser.Physics.Arcade);
	    		new_item.name = item.key;
	    		new_item.killable = false;
	    		setInterval(function(){
	    			new_item.killable = true;
	    		}, 2000);
	    	}
	    	else if(item.val().value==28){
	    		var new_item = self.items_in_use.create(item.val().ox, item.val().oy, "knife");
	    		self.game.physics.arcade.enable(new_item, Phaser.Physics.Arcade);
	    		new_item.name = item.key;
	    		self.game.physics.arcade.moveToXY(new_item, item.val().dx, item.val().dy, 600);
	    		new_item.killable = false;
	    		setInterval(function(){
	    			new_item.killable = true;
	    		}, 2000);
	    	}
	    });

	    self.item_in_use.on("child_removed", function(item){
	    	self.items_in_use.forEach(function(child){
	    		if(item.key==child.name)
	    			child.kill();
	    	});
	    });

		
	},
	update: function(){
		self = this;
		console.log(this.player.maxspeed);
		this.game.physics.arcade.collide(this.player, this.blockedLayer);
		this.game.physics.arcade.collide(this.items_in_use, this.blockedLayer, function(item, block){
			self.item_in_use.child(item.name).remove();
		});
		this.game.physics.arcade.overlap(this.player, this.items, this.collect, null, this);
		this.game.physics.arcade.overlap(this.player, this.items_in_use, this.killMethod, null, this);

		this.player.body.velocity.x = 0;
		this.player.body.velocity.y = 0;
		//this.loadToolBox();

	   	this.W = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
	    this.S = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
	    this.A = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
	    this.D = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
	    if(this.W.isDown) {
	      this.player.body.velocity.y -= this.player.maxspeed;
	    }
	    if(this.S.isDown) {
	      this.player.body.velocity.y += this.player.maxspeed;
	    }
	    if(this.A.isDown) {
	      this.player.body.velocity.x -= this.player.maxspeed;
	    }
	    if(this.D.isDown) {
	      this.player.body.velocity.x += this.player.maxspeed;
	    }
			
		if(this.player.body.velocity.x==0 && this.player.body.velocity.y==0){
			this.player.animations.play("idle");
		}
		else if(this.player.body.velocity.x==0 && this.player.body.velocity.y>0){
			this.player.animations.play("walking-down");
		}
		else if(this.player.body.velocity.x==0 && this.player.body.velocity.y<0){
			this.player.animations.play("walking-up");
		}
		else if(this.player.body.velocity.x>0){
			this.player.animations.play("walking-right");
		}
		else if(this.player.body.velocity.x<0){
			this.player.animations.play("walking-left");
		}

		if(this.game.input.activePointer.justPressed()){
			var current_tool = this.toolbox[this.player.selectBoxPosition.toString()];
			if(current_tool!=null){

				if(current_tool.key=="knife"){
					this.item_in_use.push({
						ox:this.player.x,
						oy:this.player.y,
						dx:this.game.input.activePointer.worldX,
						dy:this.game.input.activePointer.worldY,
						value: 28
					});
				}
				if(current_tool.key=="net"){
					this.item_in_use.push({
						ox:this.player.x,
						oy:this.player.y,
						dx:this.game.input.activePointer.worldX,
						dy:this.game.input.activePointer.worldY,
						value: 24
					});
				}
				if(current_tool.key=="mine"){
					this.item_in_use.push({
						ox:this.player.x,
						oy:this.player.y,
						dx:this.game.input.activePointer.worldX,
						dy:this.game.input.activePointer.worldY,
						value: 26
					});
				}
				if(current_tool.key=="glue"){
					this.item_in_use.push({
						ox:this.player.x,
						oy:this.player.y,
						dx:this.game.input.activePointer.worldX,
						dy:this.game.input.activePointer.worldY,
						value: 22
					});
				}
				if(current_tool.key=="car" ){
					this.player.maxspeed = 500;
					// this.player.speedable = false;
					// setInterval(function(){
					// 	this.player.maxspeed = 250;
					// 	this.player.speedable = true;
					// },5000);
				}
				if(current_tool.key=="shoes"){
					this.player.maxspeed = 350;
				}
				// if(current_tool.key=="shoes" && this.player.speedable==true){
				// 	this.player.maxspeed = 350;
				// 	setInterval(function(){
				// 		this.player.maxspeed = 250;
				// 		this.player.speedable = true;
				// 	},8000);
				// }
				// if(current_tool.key=="hood"){
				// 	this.currentPlayers.child(this.player.playerId).update({visible:false});
				// 	setInterval(function(){
				// 		this.currentPlayers.child(this.player.playerId).update({visible:true});
				// 	},8000);
				// }


				current_tool.destroy();
				this.toolbox[this.player.selectBoxPosition.toString()] = null;

			}
		}

		//this.player_use_item();

		this.currentPlayers.child(this.player.playerId).update({x:this.player.x, y:this.player.y});
		//console.log(Object.keys(this.items).length);
		//console.log(this.items.children.length);
	},

	random_spawn: function(){
		var row=Math.floor(Math.random()*100);
		var column=Math.floor(Math.random()*100);
		while(this.map_check_wall(row, column)){
			row=Math.floor(Math.random()*100);
			column=Math.floor(Math.random()*100);
		}
		return {x:column*150, y:row*150};
	},

	map_check_wall: function(x,y){
		if(this.blockedLayer.layer.data[x][y].index==1)
			return true;
		else
			return false;
	},

	collect:function(player, collectable){
		var row=Math.floor(Math.random()*100);
		var column=Math.floor(Math.random()*100);
		while(this.map_check_wall(row, column) || this.item_exist_in_tile(row, column)){
			row=Math.floor(Math.random()*100);
			column=Math.floor(Math.random()*100);
		}

		var item_position = this.find_toolbox_position();
		if(item_position!=-1){
			var new_toolbox_item = this.game.add.image(1600+128*item_position+12.8, 850+12.8, collectable.key);
			new_toolbox_item.scale.set(0.8,0.8);
			new_toolbox_item.fixedToCamera = true;
			this.toolbox[item_position.toString()] = new_toolbox_item;
		}

		console.log(collectable.key);
		this.item_set.child(collectable.itemId).update({row:row, column:column});
		collectable.x=column*150;
		collectable.y=row*150;

	},

	item_exist_in_tile: function(row, column){
		for(item in this.item_data){
			if(this.item_data[item].row==row && this.item_data[item].column==column){
				return true;
			}
		}

		return false;
	},

	build_player_animation:function(){
		this.player.animations.add("idle",  [25,26,27,28,29] , 5, true);
		this.player.animations.add("walking-left", [15,16,17,18,19],10,true);
		this.player.animations.add("walking-right", [5,6,7,8,9],10,true);
		this.player.animations.add("walking-up", [20,21,22,23,24],10,true);
		this.player.animations.add("walking-down", [10, 11, 12, 13, 14],10,true);
	},

	build_bot_animation:function(bot){
		bot.animations.add("idle", [0,1,2,3,2,1], 5, true);
		bot.animations.add("walking-left", [5,6,7,8,7,6], 10, true);
		bot.animations.add("walking-right", [9,10,11,12,11,10],10, true);
	},

	player_use_item:function(){

	},

	find_toolbox_position:function(){
		if(this.toolbox["0"]==null)
			return 0;

		else if(this.toolbox["1"]==null)
			return 1;

		else if(this.toolbox["2"]==null)
			return 2;

		return -1;
	},

	killMethod(player, item){
		var self = this;
		if(item.key=="mine" && item.killable==true){
			item.kill();
			self.item_in_use.child(item.name).remove();
			Criminal_Night.game.state.start('Lost');
		}
		if(item.key=="net" && item.killable==true){
			player.body.immovable = true;
			setInterval(function(){
			  player.body.immovable = false;
			}, 3000);
			item.kill();
		}
		if(item.key=="knife" && item.killable==true){
			Criminal_Night.game.state.start('Menu');
		}
		if(item.key=="glue" && item.killable==true){
			player.maxspeed = 100;
			setInterval(function(){
			  player.maxspeed = 250;
			}, 5000);
			item.kill();

		}


	}
};