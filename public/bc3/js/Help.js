var Criminal_Night = Criminal_Night || {};

Criminal_Night.Help = function(){};

//setting game configuration and loading the assets for the loading screen
Criminal_Night.Help.prototype = {
  preload: function() {
    this.game.load.spritesheet('button-back', 'assets/back.png', 200, 100);
  },
  create: function() {
    var box = this.game.add.sprite(0,200,this.game.make.bitmapData(this.game.width, this.game.height));
        console.log("entered the menu");
		box.addChild(this.game.add.text(200, 20, "HELP： ",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 100, "←:    go left",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 200, "→:    go right",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 300, "↓ :    go down",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 400, "↑ :    go up",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 500, "space bar :    select the items ",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(300, 600, "mouse left click :    use item",{fontSize:'50px',fill:'#fff'}));
        var hood=this.game.add.image(1100, 240, "hood");
        var car=this.game.add.image(1100, 340, "car");
        var shoes=this.game.add.image(1100, 440, "shoes");
        var glue=this.game.add.image(1100, 540, "glue");
        var knife=this.game.add.image(1100, 640, "knife");
        var mine=this.game.add.image(1100, 740, "mine");
        var net=this.game.add.image(1100, 840, "net");
        hood.scale.set(0.5,0.5);
        car.scale.set(0.5,0.5);
        shoes.scale.set(0.5,0.5);
        glue.scale.set(0.5,0.5);
        knife.scale.set(0.5,0.5);
        mine.scale.set(0.5,0.5);
        net.scale.set(0.5,0.5);
        box.addChild(this.game.add.text(1200, 100, "make role invisible",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 200, "drive a car",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 300, "go faster",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 400, "go slower",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 500, "kill target",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 600, "kill who is on it",{fontSize:'50px',fill:'#fff'}));
        box.addChild(this.game.add.text(1200, 700, "stun target",{fontSize:'50px',fill:'#fff'}));
        
        // box.addChild(this.game.add.text(300, 1050, "L",{fontSize:'100px',fill:'#fff'}));
        // box.addChild(this.game.add.text(300, 1200, "help",{fontSize:'100px',fill:'#fff'}));
		box.alpha = 0;
		this.game.add.tween(box).to({alpha:1,y:150},3000,"Linear",true);

		var buttonClose = this.game.add.button(50, 50, 'button-back', null, this, 0, 1, 0, 1);
		buttonClose.events.onInputDown.add(function(){
      Criminal_Night.game.state.start('Menu');
    });
    
  }
};